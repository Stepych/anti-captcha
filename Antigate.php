<?php

class Antigate
{
    private $key;
    private $proxy;
    
    public function __construct($key, $proxySOCKS5)
    {
        $this->key = $key;
        $this->proxy = $proxySOCKS5;
    }

    private function post($action, $postData) {
        $url = 'http://anti-captcha.com/'.$action;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Connection: keep-alive',
            'Pragma: no-cache',
            'Cache-Control: no-cache',
            'Accept: text/html, */*; q=0.01',
            'X-Requested-With: XMLHttpRequest',
            'User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',
            'Content-Type: application/x-www-form-urlencoded',
            'Accept-Encoding: identity',
            'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
        ]);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_PROXY, $this->proxy);
        curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);

        $result = curl_exec ($ch);
        curl_close($ch);
        return $result;
        
    }
    
    public function postCaptcha($imgBase64) {
        $imgEncoded = urlencode($imgBase64);
        $result = $this->post('in.php', 'method=base64&key='.$this->key.'&is_russian=1&body='.$imgEncoded);
        // NOTE: is_russian=1 - for cyrillic captcha, - optional param
        if (strpos($result, "ERROR") !== false)
        {
            throw new Exception("Antigate postCaptcha error: '$result'!\nimgBase64 was '$imgBase64'.");
        }
        preg_match('/OK\|(.*)/', $result, $matches);
        return $matches[1];
    }

    public function getCaptchaResult($captchaId) {
        $result =  $this->post('res.php', 'action=get&key='.$this->key.'&id='.$captchaId);
        preg_match('/OK\|(.*)/', $result, $matches);
        return $matches[1];
    }

    public function getAverageRecognitionTime() {
        $result = $this->post('load.php', '');
        preg_match('/<averageRecognitionTime>(.*)<\/averageRecognitionTime>/', $result, $matches);
        return $matches[1];
    }

    public function getAverageRecognitionTimeRU() {
        $result = $this->post('load.php', '');
        preg_match('/<averageRecognitionTimeRU>(.*)<\/averageRecognitionTimeRU>/', $result, $matches);
        return $matches[1];
    }

    public function getBalance($key = null) {
        if (is_null($key)) {
            $key = $this->key;
        }
        return $this->post("res.php?key=$key&action=getbalance", '');
    }
    
}
